*** Darwin 5x5 ***
Turn = 0.
  01234
0 r...h
1 .f.t.
2 .ht..
3 ...rf
4 f...t

Turn = 1.
  01234
0 r...h
1 .f.t.
2 .ht..
3 ..r.f
4 f...t

Turn = 2.
  01234
0 .r..h
1 .f.t.
2 .ht..
3 .r..f
4 f...t

Turn = 3.
  01234
0 ..r.h
1 .f.t.
2 .tt..
3 r...t
4 f...t

Turn = 4.
  01234
0 ...th
1 .f.t.
2 .tt..
3 r...t
4 f...t

Turn = 5.
  01234
0 ...tt
1 .t.t.
2 .tt..
3 r...t
4 r...t

Turn = 6.
  01234
0 ...tt
1 .t.t.
2 .tt..
3 r...t
4 r...t

*** Darwin 7x8 ***
Turn = 0.
  01234567
0 .r......
1 ......t.
2 ....f...
3 .....h..
4 ...r....
5 ..f.....
6 ..t....h

Turn = 1.
  01234567
0 ........
1 .r....t.
2 ....fh..
3 ........
4 ....r...
5 ..f.....
6 ..t...h.

Turn = 2.
  01234567
0 ........
1 .....ht.
2 .r..f...
3 ........
4 .....r..
5 ..f.....
6 ..t..h..

Turn = 3.
  01234567
0 .....h..
1 ......t.
2 ....f...
3 .r......
4 ......r.
5 ..t.....
6 ..t.h...

Turn = 4.
  01234567
0 .....h..
1 ......t.
2 ....f...
3 ........
4 .r.....r
5 ..t.....
6 ..th....

Turn = 5.
  01234567
0 .....h..
1 ......t.
2 ....f...
3 ........
4 .......r
5 .rt.....
6 ..th....

Turn = 6.
  01234567
0 .....h..
1 ......t.
2 ....f...
3 ........
4 ........
5 ..t....r
6 .rth....

Turn = 7.
  01234567
0 .....h..
1 ......t.
2 ....f...
3 ........
4 ........
5 ..t.....
6 .rtt...r

Turn = 8.
  01234567
0 .....h..
1 ......t.
2 ....f...
3 ........
4 ........
5 ..t.....
6 .rrr...r

*** Darwin 10x10 ***
Turn = 0.
  0123456789
0 h.........
1 .h......t.
2 .....t....
3 ...f....h.
4 ....t..r..
5 .....f....
6 ......r...
7 ....r....f
8 ..h.......
9 .r.......t

Turn = 1.
  0123456789
0 .h........
1 ........t.
2 .h...t....
3 ...f...h..
4 ....t.r...
5 .....f....
6 .....r....
7 ..h......f
8 ....r.....
9 ..r......t

Turn = 2.
  0123456789
0 ..h.......
1 ........t.
2 .....t....
3 .h.f..h...
4 ....tr....
5 .....f....
6 ..h.r.....
7 .........f
8 ..........
9 ...rr....t

*** Darwin 6x9 ***
Turn = 0.
  012345678
0 ........t
1 ...f.....
2 h.f......
3 ....h....
4 ......r..
5 .......t.

Turn = 1.
  012345678
0 ........t
1 ...f.....
2 ..f......
3 h........
4 ....hr...
5 .......t.

Turn = 2.
  012345678
0 ........t
1 ...f.....
2 ..f......
3 .........
4 h...r....
5 ....h..t.

Turn = 3.
  012345678
0 ........t
1 ...f.....
2 ..f......
3 .........
4 ...r.....
5 h...h..t.

*** Darwin 8x8 ***
Turn = 0.
  01234567
0 f.....f.
1 .r.t....
2 ..f.....
3 ......r.
4 ....th..
5 .......h
6 .r......
7 ..t..h..

Turn = 1.
  01234567
0 f.....f.
1 ..tt....
2 ..f.....
3 .....r..
4 ....t.h.
5 ........
6 ..r..h.h
7 ..t.....

Turn = 2.
  01234567
0 f.....f.
1 ..tt....
2 ..f.....
3 ....r...
4 ....t..h
5 .....h..
6 ...r....
7 ..t....h

Turn = 3.
  01234567
0 f.....f.
1 ..tt....
2 ..f.....
3 ...r....
4 ....th.h
5 ........
6 ....r...
7 ..t....h

Turn = 4.
  01234567
0 f.....f.
1 ..tt....
2 ..f.....
3 ..r..h..
4 ....t..h
5 ........
6 .....r..
7 ..t....h

Turn = 5.
  01234567
0 f.....f.
1 ..tt....
2 ..t..h..
3 .r......
4 ....t..h
5 ........
6 ......r.
7 ..t....h

Turn = 6.
  01234567
0 f.....f.
1 ..tt.h..
2 ..t.....
3 r.......
4 ....t..h
5 ........
6 .......r
7 ..t....h

Turn = 7.
  01234567
0 f....hf.
1 ..tt....
2 ..t.....
3 r.......
4 ....t..h
5 ........
6 .......r
7 ..t....h

Turn = 8.
  01234567
0 f....hf.
1 ..tt....
2 r.t.....
3 ........
4 ....t..h
5 .......r
6 ........
7 ..t....h

Turn = 9.
  01234567
0 f....hf.
1 r.tt....
2 ..t.....
3 ........
4 ....t..r
5 .......r
6 ........
7 ..t....h

*** Darwin 12x10 ***
Turn = 0.
  0123456789
0 ..........
1 ..r.......
2 .......r..
3 f.........
4 ......h...
5 .....h....
6 ........f.
7 ..........
8 .t........
9 .........t
0 ....f.....
1 ...t......

Turn = 1.
  0123456789
0 ..........
1 .......r..
2 ..r.......
3 f.........
4 .....h....
5 ..........
6 .....h..f.
7 ..........
8 .t........
9 .........t
0 ....f.....
1 ...t......

Turn = 2.
  0123456789
0 .......r..
1 ..........
2 ..........
3 f.r.......
4 ....h.....
5 ..........
6 ........f.
7 .....h....
8 .t........
9 .........t
0 ....f.....
1 ...t......

Turn = 3.
  0123456789
0 .......r..
1 ..........
2 ..........
3 f.........
4 ..rh......
5 ..........
6 ........f.
7 ..........
8 .t...h....
9 .........t
0 ....f.....
1 ...t......

Turn = 4.
  0123456789
0 ........r.
1 ..........
2 ..........
3 f.........
4 ..h.......
5 ..r.......
6 ........f.
7 ..........
8 .t........
9 .....h...t
0 ....f.....
1 ...t......

*** Darwin 5x4 ***
Turn = 0.
  0123
0 ...t
1 f...
2 .f..
3 ..h.
4 r..t

Turn = 1.
  0123
0 ...t
1 f...
2 .f..
3 r..t
4 ...t

Turn = 2.
  0123
0 ...t
1 f...
2 rf..
3 ...t
4 ...t

Turn = 3.
  0123
0 ...t
1 r...
2 rf..
3 ...t
4 ...t

Turn = 4.
  0123
0 ...t
1 r...
2 rf..
3 ...t
4 ...t

Turn = 5.
  0123
0 r..t
1 ....
2 rf..
3 ...t
4 ...t

Turn = 6.
  0123
0 r..t
1 r...
2 .f..
3 ...t
4 ...t

Turn = 7.
  0123
0 rr.t
1 ....
2 .f..
3 ...t
4 ...t

*** Darwin 9x6 ***
Turn = 0.
  012345
0 ......
1 ..f...
2 ......
3 ....h.
4 ......
5 ......
6 .f....
7 .....t
8 ...r..

Turn = 1.
  012345
0 ......
1 ..f...
2 ....h.
3 ......
4 ......
5 ......
6 .f....
7 .....t
8 ..r...

Turn = 2.
  012345
0 ......
1 ..f.h.
2 ......
3 ......
4 ......
5 ......
6 .f....
7 .....t
8 .r....

Turn = 3.
  012345
0 ....h.
1 ..f...
2 ......
3 ......
4 ......
5 ......
6 .f....
7 .....t
8 r.....

Turn = 4.
  012345
0 ....h.
1 ..f...
2 ......
3 ......
4 ......
5 ......
6 .f....
7 .....t
8 r.....

Turn = 5.
  012345
0 ....h.
1 ..f...
2 ......
3 ......
4 ......
5 ......
6 .f....
7 r....t
8 ......

Turn = 6.
  012345
0 ....h.
1 ..f...
2 ......
3 ......
4 ......
5 ......
6 rf....
7 .....t
8 ......

*** Darwin 13x7 ***
Turn = 0.
  0123456
0 ...h...
1 h......
2 ...h...
3 ....f..
4 .....f.
5 ..f....
6 .r...h.
7 ......r
8 ..t....
9 .t.....
0 ...t...
1 ......r
2 ....t..

Turn = 1.
  0123456
0 ....h..
1 .h.....
2 .......
3 ...hf..
4 .....f.
5 ..f..h.
6 r......
7 ......r
8 ..t....
9 .t.....
0 ...t...
1 .....r.
2 ....t..

Turn = 2.
  0123456
0 .....h.
1 ..h....
2 .......
3 ....f..
4 ...h.f.
5 ..f..h.
6 r......
7 .......
8 ..t...r
9 .t.....
0 ...t...
1 ....r..
2 ....t..

Turn = 3.
  0123456
0 ......h
1 ...h...
2 .......
3 ....f..
4 .....f.
5 ..fh.h.
6 .......
7 r......
8 ..t....
9 .t....r
0 ...t...
1 ...r...
2 ....t..

Turn = 4.
  0123456
0 ......h
1 ....h..
2 .......
3 ....f..
4 .....f.
5 ..f..h.
6 ...h...
7 .......
8 r.t....
9 .t.....
0 ...t..r
1 ..r....
2 ....t..

Turn = 5.
  0123456
0 ......h
1 .....h.
2 .......
3 ....f..
4 .....f.
5 ..f..h.
6 .......
7 ...h...
8 ..t....
9 tt.....
0 ...t...
1 .r....r
2 ....t..

Turn = 6.
  0123456
0 ......h
1 ......h
2 .......
3 ....f..
4 .....f.
5 ..f..h.
6 .......
7 .......
8 ..tt...
9 tt.....
0 ...t...
1 r......
2 ....t.r

Turn = 7.
  0123456
0 ......h
1 ......h
2 .......
3 ....f..
4 .....f.
5 ..f..h.
6 .......
7 .......
8 ..tt...
9 tt.....
0 ...t...
1 r......
2 ....t.r

Turn = 8.
  0123456
0 ......h
1 ......h
2 .......
3 ....f..
4 .....f.
5 ..f..h.
6 .......
7 .......
8 ..tt...
9 tt.....
0 r..t...
1 .......
2 ....tr.

*** Darwin 6x6 ***
Turn = 0.
  012345
0 ...rf.
1 .h....
2 ..h..t
3 ....f.
4 f.....
5 r....t

Turn = 1.
  012345
0 .hr.f.
1 ......
2 .....t
3 ..h.f.
4 f.....
5 r....t

Turn = 2.
  012345
0 .rr.f.
1 ......
2 .....t
3 ....f.
4 f.h...
5 r....t

*** Darwin 8x5 ***
Turn = 0.
  01234
0 ..f..
1 ...t.
2 .r.h.
3 h...t
4 f....
5 .f...
6 ....h
7 .rt..

Turn = 1.
  01234
0 ..f..
1 ...t.
2 ...t.
3 hr..t
4 f....
5 .f...
6 ....h
7 .r.r.

Turn = 2.
  01234
0 ..f..
1 ...t.
2 ...t.
3 h...t
4 fr...
5 .f...
6 ....h
7 ..r.r

Turn = 3.
  01234
0 ..f..
1 ...t.
2 ...t.
3 h...t
4 fr...
5 .r...
6 ....h
7 ...rr

Turn = 4.
  01234
0 ..f..
1 ...t.
2 ...t.
3 h...t
4 fr...
5 ..r..
6 ....r
7 ...rr

Turn = 5.
  01234
0 ..f..
1 ...t.
2 ...t.
3 h...t
4 rr...
5 ...r.
6 ....r
7 ...rr

*** Darwin 11x8 ***
Turn = 0.
  01234567
0 ........
1 .....h..
2 ........
3 ...t....
4 .......h
5 ....f...
6 ......f.
7 ........
8 ..r.....
9 ........
0 .......t

Turn = 1.
  01234567
0 ........
1 ......h.
2 ........
3 ...t....
4 ......h.
5 ....f...
6 ......f.
7 ........
8 ........
9 ..r.....
0 .......t

Turn = 2.
  01234567
0 ........
1 .......h
2 ........
3 ...t....
4 .....h..
5 ....f...
6 ......f.
7 ........
8 ........
9 ........
0 ..r....t

Turn = 3.
  01234567
0 ........
1 .......h
2 ........
3 ...t....
4 ....h...
5 ....f...
6 ......f.
7 ........
8 ........
9 ........
0 ..r....t

*** Darwin 14x10 ***
Turn = 0.
  0123456789
0 ..........
1 ..........
2 .........f
3 .r........
4 ....t.....
5 ...h......
6 ..........
7 ......t...
8 ..........
9 h.........
0 .......f..
1 ..........
2 ........f.
3 ..........

Turn = 1.
  0123456789
0 ..........
1 ..........
2 .........f
3 ..r.......
4 ....t.....
5 ....h.....
6 ..........
7 ......t...
8 ..........
9 ..........
0 h......f..
1 ..........
2 ........f.
3 ..........

Turn = 2.
  0123456789
0 ..........
1 ..........
2 .........f
3 ...r......
4 ....t.....
5 ....t.....
6 ..........
7 ......t...
8 ..........
9 ..........
0 .......f..
1 h.........
2 ........f.
3 ..........

Turn = 3.
  0123456789
0 ..........
1 ..........
2 .........f
3 ....r.....
4 ....t.....
5 ....t.....
6 ..........
7 ......t...
8 ..........
9 ..........
0 .......f..
1 ..........
2 h.......f.
3 ..........

Turn = 4.
  0123456789
0 ..........
1 ..........
2 .........f
3 .....r....
4 ....t.....
5 ....t.....
6 ..........
7 ......t...
8 ..........
9 ..........
0 .......f..
1 ..........
2 ........f.
3 h.........

*** Darwin 9x9 ***
Turn = 0.
  012345678
0 .........
1 h........
2 .........
3 ...f.....
4 .....t...
5 .........
6 .......r.
7 ..f......
8 ....t....

Turn = 1.
  012345678
0 h........
1 .........
2 .........
3 ...f.....
4 .....t...
5 .........
6 ......r..
7 ..f......
8 ....t....

Turn = 2.
  012345678
0 h........
1 .........
2 .........
3 ...f.....
4 .....t...
5 .........
6 .....r...
7 ..f......
8 ....t....

Turn = 3.
  012345678
0 h........
1 .........
2 .........
3 ...f.....
4 .....t...
5 .........
6 ....r....
7 ..f......
8 ....t....

Turn = 4.
  012345678
0 h........
1 .........
2 .........
3 ...f.....
4 .....t...
5 .........
6 ...r.....
7 ..f......
8 ....t....

Turn = 5.
  012345678
0 h........
1 .........
2 .........
3 ...f.....
4 .....t...
5 .........
6 ..r......
7 ..f......
8 ....t....

*** Darwin 10x7 ***
Turn = 0.
  0123456
0 .h.....
1 ..f....
2 ......r
3 ...t...
4 ......r
5 .....t.
6 .h.....
7 ....h..
8 ...f...
9 t......

Turn = 1.
  0123456
0 .h.....
1 ..f....
2 ......r
3 ...t...
4 .....t.
5 .....t.
6 h...h..
7 .......
8 ...f...
9 t......

Turn = 2.
  0123456
0 .h.....
1 ..f....
2 .......
3 ...t..r
4 .....t.
5 ....ht.
6 h......
7 .......
8 ...f...
9 t......

Turn = 3.
  0123456
0 .h.....
1 ..f....
2 .......
3 ...t...
4 ....htr
5 .....t.
6 h......
7 .......
8 ...f...
9 t......

Turn = 4.
  0123456
0 .h.....
1 ..f....
2 .......
3 ...th..
4 .....tt
5 .....t.
6 h......
7 .......
8 ...f...
9 t......

Turn = 5.
  0123456
0 .h.....
1 ..f....
2 .......
3 ...tt..
4 .....tt
5 .....t.
6 h......
7 .......
8 ...f...
9 t......

Turn = 6.
  0123456
0 .h.....
1 ..f....
2 .......
3 ...tt..
4 .....tt
5 .....t.
6 h......
7 .......
8 ...f...
9 t......
