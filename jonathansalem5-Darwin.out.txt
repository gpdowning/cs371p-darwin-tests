*** Darwin 13x11 ***
Turn = 0.
  01234567890
0 ...........
1 ht.h.......
2 f..f......f
3 f..t.......
4 .....h....r
5 ........tf.
6 f..........
7 ........th.
8 f..........
9 ..h.h.r..t.
0 ...........
1 ....f......
2 ...........

Turn = 3.
  01234567890
0 ...........
1 ht....h....
2 f..t......f
3 f..t.......
4 ........t..
5 ........tt.
6 f.........r
7 ........t.r
8 f..........
9 h..r.....t.
0 ....h......
1 ....f......
2 ...........

Turn = 6.
  01234567890
0 ...........
1 tt.......h.
2 t..t......f
3 f..t.......
4 ........t..
5 ........tt.
6 f..........
7 ........t.r
8 f..........
9 rr.......tt
0 ....h......
1 ....f......
2 ...........

Turn = 9.
  01234567890
0 ...........
1 tt........h
2 t..t......f
3 t..t.......
4 ........t..
5 ........tt.
6 f..........
7 .r......t..
8 r.........r
9 r........tr
0 ....h......
1 ....f......
2 ...........

Turn = 12.
  01234567890
0 ...........
1 tt........h
2 t..t......f
3 t..t.......
4 .r......t..
5 r.......tt.
6 r.........r
7 ........t..
8 ...........
9 ...r.....tt
0 ....h......
1 ....f......
2 ...........

Turn = 15.
  01234567890
0 ...........
1 tr........h
2 tr.t......f
3 t..t......r
4 r.......t..
5 r.......tt.
6 ...........
7 ........t..
8 ...........
9 ......r..tt
0 ....h......
1 ....f......
2 ...........

Turn = 18.
  01234567890
0 ...........
1 tt........r
2 tt.t....r.r
3 t..t.......
4 t.......t..
5 t.......tt.
6 ...........
7 ........t..
8 ...........
9 ........ttt
0 ....h......
1 ....f......
2 ...........

*** Darwin 6x5 ***
Turn = 0.
  01234
0 r..ht
1 thr..
2 ..t.r
3 r.rf.
4 .hhrr
5 .t..h

Turn = 1.
  01234
0 rhh.t
1 t..r.
2 ..t..
3 r.rfr
4 .t.r.
5 .trrh

Turn = 2.
  01234
0 rhh.t
1 t...r
2 ..t..
3 ...f.
4 rtrrr
5 .trrh

Turn = 3.
  01234
0 trh.t
1 t...r
2 ..t..
3 ...r.
4 .ttrr
5 ttrrr

Turn = 4.
  01234
0 tth.t
1 t...t
2 ..t..
3 ..rr.
4 .tt.r
5 ttrrr

Turn = 5.
  01234
0 tth.t
1 t...t
2 ..tr.
3 .t...
4 .ttr.
5 tttrr

Turn = 6.
  01234
0 ttt.t
1 t..rt
2 ..t..
3 .t...
4 .trr.
5 tttrr

Turn = 7.
  01234
0 tttrt
1 t...t
2 ..t..
3 .t...
4 .trr.
5 ttttt

Turn = 8.
  01234
0 ttttt
1 t...t
2 ..t..
3 .t...
4 .trr.
5 ttrrr

Turn = 9.
  01234
0 ttttt
1 t...t
2 ..t..
3 .t...
4 .ttr.
5 tttrr

Turn = 10.
  01234
0 ttttt
1 t...t
2 ..t..
3 .t...
4 .tt.r
5 ttttr

Turn = 11.
  01234
0 ttttt
1 t...t
2 ..t..
3 .t...
4 .tt.r
5 ttttr

Turn = 12.
  01234
0 ttttt
1 t...t
2 ..t..
3 .t..r
4 .tt..
5 ttttr

Turn = 13.
  01234
0 ttttt
1 t...t
2 ..t.r
3 .t...
4 .tt..
5 ttttt

*** Darwin 9x9 ***
Turn = 0.
  012345678
0 h......f.
1 .f....r.r
2 .h.......
3 ....t....
4 ..f......
5 ..r...r.r
6 .t.......
7 .f.....tf
8 .h..t..hh

Turn = 3.
  012345678
0 ...h...f.
1 .f.r.r...
2 ......r..
3 ....t....
4 ..f......
5 rh.......
6 .t......r
7 .t.....tr
8 .h..t..tt

*** Darwin 12x11 ***
Turn = 0.
  01234567890
0 ..........r
1 ...rft.....
2 r...f......
3 ...r.......
4 .r....f...t
5 f....t.t...
6 ...f.....r.
7 ..h........
8 ...........
9 ......f..t.
0 ......f....
1 ........r..

Turn = 2.
  01234567890
0 r.......r..
1 ....ft.....
2 ...rf......
3 ....r......
4 ......f...t
5 f....t.t...
6 .r.f.......
7 ....h......
8 .........r.
9 ......f..t.
0 ......f....
1 ......r....

Turn = 4.
  01234567890
0 .r....r....
1 ....tt.....
2 ...rr......
3 ......r....
4 ......f...t
5 f....t.t...
6 ...f.......
7 ......h....
8 .r.........
9 ......f..rr
0 ......f....
1 ....r......

Turn = 6.
  01234567890
0 ...rr......
1 ...rrt.....
2 ...........
3 ........r..
4 ...r..f...t
5 f....t.t...
6 ...f.......
7 ........h.r
8 ...........
9 ......f....
0 .r....f....
1 ..r......r.

Turn = 8.
  01234567890
0 ...rr......
1 .r..rr.....
2 ...........
3 ..........r
4 ......f...t
5 f..r.t.t..r
6 ...r.......
7 ..........h
8 ...........
9 ......f....
0 ......f....
1 .rr.....r..

Turn = 10.
  01234567890
0 .r..r.r....
1 r...r......
2 ...........
3 ..........r
4 ......f...r
5 f...rt.t.r.
6 .r.........
7 ..........h
8 ...........
9 ......f....
0 ......f....
1 .r..r.r....

Turn = 12.
  01234567890
0 r..r....r..
1 r.r........
2 ...........
3 .........r.
4 ......f....
5 f....rrrr..
6 r.........r
7 ..........h
8 ...........
9 ......f....
0 ......r....
1 ...r.rr....

Turn = 14.
  01234567890
0 .rr.......r
1 rr.........
2 ...........
3 .......r...
4 .....rf....
5 f..........
6 ......r...r
7 .......rr.r
8 r..........
9 .....rr....
0 ...........
1 ....rr.r...

Turn = 16.
  01234567890
0 .r.r......r
1 ...........
2 .....r.....
3 rr...r.....
4 ......f....
5 f..........
6 ........r..
7 ......r.r..
8 ......r....
9 ...rrr.rr..
0 r..........
1 .........r.

*** Darwin 4x11 ***
Turn = 0.
  01234567890
0 .t..ff.hr..
1 hfr.h.h....
2 hr.t....h..
3 ..r..h..rt.

Turn = 1.
  01234567890
0 .t..ffhhr..
1 htt..h.....
2 h.rt...hr..
3 .r....h..t.

Turn = 2.
  01234567890
0 .t..ffhh.r.
1 htr...h.r..
2 h.rt..h....
3 .r.....h.t.

Turn = 3.
  01234567890
0 .t..ffhhr.r
1 hrr....h...
2 h.tt.h.....
3 ..r.....ht.

Turn = 4.
  01234567890
0 .t..ffhhr.r
1 rrr.....h..
2 h.tth......
3 ...r....ht.

Turn = 5.
  01234567890
0 .tr.ffhh.r.
1 rr.......hr
2 h.tth......
3 ...t....ht.

Turn = 6.
  01234567890
0 .tr.ffhh..r
1 rt.......h.
2 r.tth.....r
3 ...t....tt.

Turn = 7.
  01234567890
0 .t.rffhh..r
1 rt........h
2 .rttt......
3 ...t....ttr

Turn = 8.
  01234567890
0 .t.r.fhh..r
1 rt..r.....h
2 .rrr.......
3 ...tr...ttr

Turn = 9.
  01234567890
0 rt..rfhh.r.
1 .t........h
2 .rrrr......
3 ...tt...ttt

Turn = 10.
  01234567890
0 tt..rrrhr..
1 .trr......h
2 .t..r......
3 ...tt...ttt

Turn = 11.
  01234567890
0 ttrrrrrrr..
1 .t........h
2 .t..t......
3 ...tt...ttt

Turn = 12.
  01234567890
0 ttrr.rrrr..
1 .t..r.....h
2 .t..t......
3 ...tt...ttt

Turn = 13.
  01234567890
0 ttttrr.rr..
1 .t..r.....h
2 .t...r.....
3 ...tt...ttt

*** Darwin 7x5 ***
Turn = 0.
  01234
0 ..thf
1 f..t.
2 .h..r
3 ff...
4 .thf.
5 r..th
6 .rtth

Turn = 2.
  01234
0 ..ttr
1 f..tr
2 h....
3 rt..h
4 rt.t.
5 ..ht.
6 r.ttt

Turn = 4.
  01234
0 ..ttt
1 f..tt
2 h...h
3 tt...
4 rr.t.
5 r.tt.
6 ..ttt

Turn = 6.
  01234
0 ..ttt
1 f..tt
2 h...t
3 tt...
4 ttrt.
5 .r.t.
6 ..ttt

*** Darwin 10x11 ***
Turn = 0.
  01234567890
0 ...f.......
1 t....r.....
2 ..r........
3 .r.r.......
4 f.....t.t..
5 ..r..tfff..
6 ......f....
7 ....h.....f
8 ......t....
9 ...........

Turn = 4.
  01234567890
0 .r.f.......
1 tr.........
2 ......r....
3 r..........
4 f.....r.t..
5 ....rrttt..
6 ......f....
7 ..........f
8 ......t....
9 ....h......

Turn = 8.
  01234567890
0 r...r......
1 ...........
2 .......r..r
3 .r.r.......
4 ....rr..t..
5 ........t..
6 r...r.r....
7 ..........f
8 ....r.t....
9 ....rr.....

Turn = 12.
  01234567890
0 .....r..r..
1 r...r.r....
2 ...........
3 ...........
4 ........t..
5 ........t.r
6 ....r......
7 .rrr.r....f
8 ......r....
9 r....rr....

Turn = 16.
  01234567890
0 ...........
1 ....r.....r
2 ....rr.....
3 .....r.....
4 ........t..
5 r.....r.t..
6 .........r.
7 ...........
8 r.r.r.....r
9 r.rr.r.....

Turn = 20.
  01234567890
0 ...........
1 .......r...
2 .rr........
3 ..r........
4 ....r...t..
5 ...r....t.r
6 .....r.....
7 r.r........
8 r..........
9 r..rr.r.r..

*** Darwin 4x11 ***
Turn = 0.
  01234567890
0 .....f..frr
1 ...f......t
2 trt......r.
3 rf.t.....h.

Turn = 3.
  01234567890
0 .....f..frt
1 ...f......t
2 trt.......r
3 rr.t......r

Turn = 6.
  01234567890
0 .....f..frr
1 ...f......r
2 trt.....r..
3 rr.t....r..

Turn = 9.
  01234567890
0 .....f..rr.
1 ...f.......
2 trt..r....r
3 rr.t.r....r

Turn = 12.
  01234567890
0 .....f....r
1 ...f.......
2 trtr......r
3 rr.rr....rr

Turn = 15.
  01234567890
0 .....f.....
1 ...f......r
2 trrr.....r.
3 rrr...r..rr

*** Darwin 11x9 ***
Turn = 0.
  012345678
0 .......h.
1 ....h.r..
2 ..r....h.
3 h...f.t.t
4 .r......t
5 ..r.h.hf.
6 f....f...
7 ...rt....
8 .h.......
9 .........
0 ft....t..

Turn = 2.
  012345678
0 ....h....
1 ....r....
2 .......h.
3 h...f.t.t
4 ..r...hht
5 ..rh...f.
6 fr...f...
7 .h..r....
8 .........
9 ....r....
0 ft....t..

Turn = 4.
  012345678
0 ....h....
1 ..r......
2 .........
3 h...f.tht
4 .r....tht
5 rh.....f.
6 fr...f...
7 ..r...r..
8 .........
9 .........
0 tt..r.t..

Turn = 6.
  012345678
0 ....h....
1 r........
2 .........
3 h...f.ttt
4 r.....ttt
5 rh.....t.
6 rr...f...
7 ....r...r
8 .........
9 .........
0 ttr...t..

Turn = 8.
  012345678
0 r...h....
1 .........
2 .........
3 h...f.ttt
4 rr....ttt
5 h......t.
6 .....f...
7 ......r..
8 rr......r
9 .........
0 ttt...t..

Turn = 10.
  012345678
0 r...h....
1 .........
2 .........
3 r...f.ttt
4 r..r..ttt
5 h......t.
6 .....f...
7 ........r
8 .........
9 tt.......
0 ttt...t.r

Turn = 12.
  012345678
0 ....h....
1 .........
2 r........
3 rr..f.ttt
4 .....tttt
5 h......t.
6 .....f..r
7 .........
8 .........
9 tt.......
0 ttt...tr.

Turn = 14.
  012345678
0 ....h....
1 .........
2 r........
3 r..rf.ttt
4 .....tttt
5 h......tt
6 .....f...
7 .........
8 .........
9 tt.......
0 ttt...tt.

Turn = 16.
  012345678
0 ....h....
1 .........
2 r........
3 ....r.ttt
4 r...ttttt
5 r......tt
6 .....f...
7 .........
8 .........
9 tt.......
0 ttt...tt.

Turn = 18.
  012345678
0 r...h....
1 .........
2 .........
3 .....rrtt
4 r...tttrt
5 r......rt
6 .....f...
7 .........
8 .........
9 tt.......
0 ttt...tt.

Turn = 20.
  012345678
0 .r..h....
1 .........
2 .........
3 .....trtt
4 .r..ttttt
5 .......tt
6 .....f...
7 r........
8 .........
9 tt.......
0 ttt...tt.

*** Darwin 10x10 ***
Turn = 0.
  0123456789
0 .t.....t..
1 .r.tt.....
2 .........t
3 ......r...
4 ....h.....
5 .h....hh..
6 .....h....
7 ....f...th
8 .....r.h.r
9 .......h..

Turn = 4.
  0123456789
0 .t.....t..
1 .t.tt..h..
2 .........t
3 .....h....
4 ..........
5 ..h.......
6 ....h.....
7 ....f.r.tt
8 .r.......h
9 .h......hr

Turn = 8.
  0123456789
0 .t...h.t..
1 .t.tt..t..
2 .........t
3 ..........
4 ..........
5 h.........
6 r...h.....
7 ....f...tt
8 ........rt
9 .h...r...t

*** Darwin 6x8 ***
Turn = 0.
  01234567
0 tf.ff..h
1 .rhf..t.
2 t.t..r..
3 ...h..hr
4 ....r...
5 ..h...rf

Turn = 1.
  01234567
0 tt.ff..h
1 .thf..t.
2 t.th...r
3 .....rh.
4 ......r.
5 ...hr..f

Turn = 2.
  01234567
0 tt.ff..h
1 .ttt..tr
2 t.tt....
3 ......rh
4 .....r..
5 ...hr..f

Turn = 3.
  01234567
0 tt.ff..t
1 .ttt..tt
2 t.tt..r.
3 .......h
4 ........
5 ...rrr.f

Turn = 4.
  01234567
0 tt.ff..t
1 .ttt..rt
2 t.tt..r.
3 .......h
4 ........
5 ...rrr.f

Turn = 5.
  01234567
0 tt.tf.rt
1 .ttt..rt
2 t.tt....
3 .......h
4 ...rr...
5 ....r..f

Turn = 6.
  01234567
0 tt.tf.rt
1 .ttt..rt
2 t.tt....
3 ...rr..h
4 ........
5 ...r...f

Turn = 7.
  01234567
0 tt.tf.rr
1 .ttt..rt
2 t.trr...
3 ...r...h
4 ........
5 ..r....f

Turn = 8.
  01234567
0 tt.tf.rt
1 .tttrr.t
2 t.tt....
3 ...t...h
4 ........
5 .r.....f

Turn = 9.
  01234567
0 tt.tr.rt
1 .tttrr.t
2 t.tt....
3 ...t...h
4 ........
5 r......f

Turn = 10.
  01234567
0 tt.tt.rr
1 .tttt..t
2 t.tt.r..
3 ...t...h
4 ........
5 r......f

Turn = 11.
  01234567
0 tt.tt.rr
1 .tttt..t
2 t.tt....
3 ...t.r.h
4 ........
5 r......f

Turn = 12.
  01234567
0 tt.tt..r
1 .tttt.rr
2 t.tt....
3 ...t...h
4 .....r..
5 .r.....f

Turn = 13.
  01234567
0 tt.tt..r
1 .tttt..r
2 t.tt..r.
3 ...t...h
4 ........
5 ..r..r.f

Turn = 14.
  01234567
0 tt.tt..r
1 .tttt.r.
2 t.tt....
3 ...t..rh
4 ........
5 ...r.r.f

Turn = 15.
  01234567
0 tt.tt..r
1 .ttttr..
2 t.tt....
3 ...t...h
4 ......r.
5 ....rr.f

Turn = 16.
  01234567
0 tt.tt.r.
1 .tttrr..
2 t.tt....
3 ...t...h
4 .....r..
5 ....r.rf

Turn = 17.
  01234567
0 tt.ttr..
1 .tttrr..
2 t.tt....
3 ...t.r.h
4 ........
5 ....r.rf

Turn = 18.
  01234567
0 tt.ttr..
1 .tttt...
2 t.tt.r..
3 ...t.r.h
4 ........
5 .....rrf

*** Darwin 7x7 ***
Turn = 0.
  0123456
0 ....fr.
1 rhtf.t.
2 r.fht..
3 ..h.f..
4 t.rt...
5 t.h...f
6 ..h..r.

Turn = 1.
  0123456
0 r...rt.
1 ..tf.t.
2 rrf.t..
3 ..hhf..
4 tr.t...
5 t.h..rf
6 .h.....

Turn = 2.
  0123456
0 r...tt.
1 ..tf.t.
2 r.f.t..
3 .r.hf..
4 rrht.r.
5 t.....f
6 h.h....

Turn = 3.
  0123456
0 .r..tt.
1 ..tf.t.
2 ..t.t..
3 rr.tfr.
4 tr.t...
5 t.h...f
6 h.h....

Turn = 4.
  0123456
0 ..r.tt.
1 ..tf.t.
2 ..t.tr.
3 rr.tt..
4 rr.t...
5 r.h...f
6 h.h....

Turn = 5.
  0123456
0 ...ttt.
1 ..tt.r.
2 ..t.tr.
3 rr.tt..
4 rr.t...
5 .rh...f
6 h.h....

Turn = 6.
  0123456
0 ...ttr.
1 ..tt.t.
2 ..t.tt.
3 r.rtt..
4 rr.t...
5 .rr...f
6 h.r....

Turn = 7.
  0123456
0 ...trt.
1 ..tt.t.
2 r.t.tt.
3 r.rtt..
4 ..rt...
5 .rr...f
6 h..r...

Turn = 8.
  0123456
0 ...t.t.
1 r.tttt.
2 r.t.tt.
3 ..trt..
4 .rrr...
5 ...r..f
6 h...r..

Turn = 9.
  0123456
0 r..t.t.
1 r.tttt.
2 ..t.tt.
3 .rtr...
4 ..rrr..
5 ....r.f
6 h....r.

Turn = 10.
  0123456
0 r..t.t.
1 r.tttt.
2 .rt.tt.
3 ..t.r..
4 ..r.r..
5 ..r..rf
6 h.....r

Turn = 11.
  0123456
0 .r.t.t.
1 .rtttt.
2 .rt.tt.
3 ..t.t..
4 ..t..r.
5 .....rr
6 h.r...r

*** Darwin 4x7 ***
Turn = 0.
  0123456
0 ..t.h.f
1 t.tht..
2 ff...hf
3 ..rhrrh

Turn = 4.
  0123456
0 ..th..r
1 t.trr.r
2 ttrrr.r
3 ..r...r

Turn = 8.
  0123456
0 ..tttrr
1 t.tt..r
2 tttrr..
3 t.....r

Turn = 12.
  0123456
0 ..ttttt
1 t.tt..t
2 ttttt..
3 t.r....

Turn = 16.
  0123456
0 ..ttttt
1 t.tt..t
2 ttttt..
3 t.t....

Turn = 20.
  0123456
0 ..ttttt
1 t.tt..t
2 ttttt..
3 t.t....

*** Darwin 9x6 ***
Turn = 0.
  012345
0 ..h..f
1 h.fttr
2 .t.f.r
3 ....h.
4 ..r...
5 .fh...
6 t.....
7 ......
8 t....h

Turn = 4.
  012345
0 hh...f
1 ..trrr
2 .t.t.r
3 .....r
4 .....r
5 .f....
6 t.....
7 ......
8 t.h..h

Turn = 8.
  012345
0 hhr..r
1 ..ttt.
2 .t.rr.
3 ...r..
4 ......
5 .f....
6 t.....
7 .....r
8 t.h..r

*** Darwin 7x14 ***
Turn = 0.
  01234567890123
0 .f....f...hrr.
1 ..r....tr....h
2 .....r........
3 .t.........rtt
4 .h..h.t.......
5 ..r.....f..h..
6 ........f.....

Turn = 2.
  01234567890123
0 .f....f.h....r
1 ....r..t...h..
2 ...r.......rr.
3 .t......r..r.t
4 ......t.......
5 ........fh....
6 .hr.h...f.....

Turn = 4.
  01234567890123
0 .f....ft....rr
1 ......rt.h....
2 .t........r...
3 .t...........t
4 ......t.r.....
5 .......rh..r..
6 .rr.h...f.....

Turn = 6.
  01234567890123
0 .f....tr....rr
1 ......rrh.....
2 .t......r.....
3 .t...........t
4 ..r...t.......
5 .....rr.r.....
6 .r..h...f..r..

Turn = 8.
  01234567890123
0 .f....tt...rr.
1 .......th.....
2 .t....r.......
3 .tt...r......t
4 ......t.......
5 ...rr.........
6 ...rh.r.r....r

Turn = 10.
  01234567890123
0 .f....tt.rr...
1 .......tt.....
2 .t..r.........
3 .tt..........t
4 ......r.......
5 .rr..........r
6 ...rrrrr......

Turn = 12.
  01234567890123
0 .f....ttrr....
1 .......tt.....
2 .tt...........
3 .tt..........r
4 .............r
5 rr....rr......
6 ...rrrr.......

Turn = 14.
  01234567890123
0 .f....ttt.r...
1 .......tt....r
2 .tt..........r
3 .tt....r......
4 .....rr.......
5 ..............
6 rrrr.r........

Turn = 16.
  01234567890123
0 .f....ttt...rr
1 .......rt....r
2 .tt..rrr......
3 .tt...........
4 ..............
5 rr.r..........
6 ..rr..........

Turn = 18.
  01234567890123
0 .f...rrtt....r
1 ......trt...rr
2 .tt....r......
3 rrtr..........
4 .r............
5 ..............
6 ..rr..........
